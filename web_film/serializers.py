from rest_framework import serializers
from .models import *


class FilmSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilmModel
        fields = '__all__'


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthorModel
        fields = '__all__'


class ActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActorModel
        fields = '__all__'


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = GenreModel
        fields = '__all__'


class FilmThSerializer(serializers.ModelSerializer):
    class Meta:
        model = EpisodeModel
        fields = '__all__'
