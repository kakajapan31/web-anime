from django.views import generic
from django.shortcuts import redirect
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework import generics
from .serializers import *


def logout(request):
    auth_logout(request)
    messages.info(request, "See you later!")
    return redirect('index')


class FilmListView(generic.ListView):
    template_name = 'web_film/index.html'
    context_object_name = 'list_film'
    model = FilmModel

    def get_queryset(self):
        if 'search' in self.request.GET:
            query = self.request.GET['search']
            return FilmModel.objects.filter(name__icontains=query)
        return FilmModel.objects.all()[:8]


class FilmView(generic.DetailView):
    model = FilmModel
    template_name = "web_film/film_detail.html"
    context_object_name = 'film'
    # print(FilmModel.objects.first().author.all())

    def post(self, request, pk):
        if request.POST.get('watch'):
            film_now = self.get_object()
            film_now.view_amount += 1
            film_now.save()
            return redirect('film_page', film_now.episodes.first().id)


class AuthorView(generic.DetailView):
    model = AuthorModel
    template_name = 'web_film/author.html'
    context_object_name = 'author'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_film'] = FilmModel.objects.filter(author__in=[self.object])
        return context


class ActorView(generic.DetailView):
    model = ActorModel
    template_name = 'web_film/actor.html'
    context_object_name = 'actor'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_film'] = FilmModel.objects.filter(actor__in=[self.object])
        return context


class GenreView(generic.DetailView):
    model = GenreModel
    template_name = 'web_film/genre.html'
    context_object_name = 'genre'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_film'] = FilmModel.objects.filter(genre__in=[self.object])
        return context


class FilmPageView(generic.DetailView):
    template_name = 'web_film/film_page.html'
    model = EpisodeModel
    context_object_name = 'number_of_film'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        film = self.object.film
        context['list_film'] = film.episodes.all().order_by('numberth')
        context['film'] = film
        return context


class AboutMe(generic.TemplateView):
    template_name = 'web_film/about_me.html'


class Login(generic.TemplateView):
    template_name = 'web_film/login.html'

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        next = request.GET.get('next', 'index')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            messages.info(request, "Welcome " + user.first_name + " " + user.last_name + " to come back ")
            return redirect(next)
        else:
            messages.error(request, 'Username or password not correct, please try again.')
            return redirect(request.get_full_path())

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('index')
        return super().dispatch(*args, **kwargs)


class Register(generic.TemplateView):
    template_name = 'web_film/register.html'

    def post(self, request):
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        email = request.POST['email']

        if password1 != password2:
            messages.error(request, "Your passwords didn't match.")
            return redirect('register')
        elif len(password1) < 6:
            messages.error(request, "Your password must be more than or equal 6 characters")
            return redirect('register')
        elif User.objects.filter(email=email).exists():
            messages.error(request, "Your email is used.")
            return redirect('register')
        elif User.objects.filter(username=username).exists():
            messages.error(request, "Your username is used.")
            return redirect('register')
        else:
            User.objects.create_user(username=username, email=email, password=password1, first_name=first_name,
                                     last_name=last_name)
        user = authenticate(request, username=username, password=password1)
        if user is not None:
            auth_login(request, user)
            messages.info(request, "Welcome new member " + user.first_name + " " + user.last_name)
            return redirect('index')

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('index')
        return super().dispatch(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class MyAccount(generic.TemplateView):
    template_name = 'web_film/my_account.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


@method_decorator(login_required, name='dispatch')
class ChangePassword(generic.TemplateView):
    template_name = 'web_film/change_password.html'

    def post(self, request):
        user = request.user
        current_password = request.POST['current_password']

        user_temp = authenticate(request=request, username=user.username, password=current_password)
        if user_temp is None:
            messages.error(request, "Current password is wrong!")
            return redirect('change_password')

        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if password1 != password2:
            messages.error(request, "Password does not match!")
            return redirect('change_password')
        elif len(password1) < 6:
            messages.error(request, "The length of your password must be more than or equal 6 characters")
            return redirect('change_password')

        user.set_password(password1)
        user.save()
        messages.success(request, "You have changed your password successful!")
        user = authenticate(request=request, username=user.username, password=password1)
        auth_login(request, user)
        return redirect('change_password')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ForgotPassword(generic.TemplateView):
    template_name = 'web_film/forgot_password.html'

    def post(self, request):
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        try:
            user = User.objects.get(username=username, email=email)
            if password1 != password2:
                messages.error(request, "Your password did not match")
                return redirect('forgot_password')
            else:
                messages.success(request, "You get new password successful")
                user.set_password(password1)
                user.save()
                user = authenticate(request=request, username=user.username, password=password1)
                auth_login(request=request, user=user)
                return redirect('forgot_password')
        except User.DoesNotExist:
            messages.error(request, "Your username or your email is wrong, try again!")
            return redirect('forgot_password')


@method_decorator(login_required, name='dispatch')
class Help(generic.TemplateView):
    template_name = 'web_film/help.html'

    def post(self, request):
        question = request.POST['question']
        if question != "":
            messages.success(request, "Thanks for your question, I will reply you soon!")
        else:
            messages.error(request, "You have not asked me :(")
        return redirect('help')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class FilmListCreateView(generics.ListCreateAPIView):
    queryset = FilmModel.objects.all()[:10]
    serializer_class = FilmSerializer


class FilmDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = FilmModel.objects.all()
    serializer_class = FilmSerializer


class AuthorListView(generics.ListCreateAPIView):
    queryset = AuthorModel.objects.all()[:10]
    serializer_class = AuthorSerializer


class AuthorDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = AuthorModel.objects.all()
    serializer_class = AuthorSerializer


class ActorListView(generics.ListCreateAPIView):
    queryset = ActorModel.objects.all()[:10]
    serializer_class = ActorSerializer


class ActorDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ActorModel.objects.all()
    serializer_class = ActorSerializer


class GenreListView(generics.ListCreateAPIView):
    queryset = GenreModel.objects.all()[:10]
    serializer_class = GenreSerializer


class GenreDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GenreModel.objects.all()
    serializer_class = GenreSerializer


class FitmThListView(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = EpisodeModel.objects.filter(film=self.kwargs["pk"])
        return queryset

    serializer_class = FilmThSerializer


class FilmThDetailOfFilm(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        film_set = EpisodeModel.objects.filter(film=self.kwargs["pk"])
        queryset = film_set.filter(numberth=self.kwargs["film_pk"])
        return queryset

    serializer_class = FilmThSerializer
