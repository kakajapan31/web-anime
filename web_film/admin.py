from django.contrib import admin

# Register your models here.
from .models import *


class FilmGenreAdmin(admin.TabularInline):
    model = FilmGenreModel
    extra = 1


class FilmActorAdmin(admin.TabularInline):
    model = FilmActorModel
    extra = 1


class FilmAuthorAdmin(admin.TabularInline):
    model = FilmAuthorModel
    extra = 1


class AuthorGenreAdmin(admin.TabularInline):
    model = AuthorGenreModel
    extra = 1


class ActorGenreAdmin(admin.TabularInline):
    model = ActorGenreModel
    extra = 1


class FilmAdmin(admin.ModelAdmin):
    inlines = (FilmGenreAdmin, FilmActorAdmin, FilmAuthorAdmin)


class GenreAdmin(admin.ModelAdmin):
    inlines = (FilmGenreAdmin, AuthorGenreAdmin, ActorGenreAdmin)


class ActorAdmin(admin.ModelAdmin):
    inlines = (FilmActorAdmin, ActorGenreAdmin)


class AuthorAdmin(admin.ModelAdmin):
    inlines = (FilmAuthorAdmin, AuthorGenreAdmin)


admin.site.register(FilmModel, FilmAdmin)
admin.site.register(ActorModel, ActorAdmin)
admin.site.register(AuthorModel, AuthorAdmin)
admin.site.register(GenreModel, GenreAdmin)
# admin.site.register(CommentModel)
admin.site.register(EpisodeModel)
# admin.site.register(FilmGenreModel)
# admin.site.register(FilmAuthorModel)
# admin.site.register(FilmActorModel)
