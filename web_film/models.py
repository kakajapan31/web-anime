from django.db import models


class GenreModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    image = models.URLField(null=True)

    class Meta:
        db_table = 'genre'
        managed = False

    def __str__(self):
        return self.name


class AuthorModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    date_of_birth = models.IntegerField(default=0)
    country = models.CharField(max_length=50, default='', blank=True)
    image = models.URLField(null=True)
    height = models.CharField(max_length=10, default='', blank=True)
    good_point = models.CharField(max_length=100, default='', blank=True)
    like_point = models.CharField(max_length=100, default='', blank=True)
    genre = models.ManyToManyField(GenreModel, through='AuthorGenreModel', through_fields=('authors', 'genres'))

    class Meta:
        db_table = 'author'
        managed = False

    def __str__(self):
        return self.name


class AuthorGenreModel(models.Model):
    id = models.AutoField(primary_key=True)
    genres = models.ForeignKey(GenreModel, on_delete=models.CASCADE, db_column='genre_id')
    authors = models.ForeignKey(AuthorModel, on_delete=models.CASCADE, db_column='author_id')

    class Meta:
        db_table = 'author_genre'
        unique_together = (('authors', 'genres'),)
        managed = False


class ActorModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    date_of_birth = models.IntegerField(default=0)
    country = models.CharField(max_length=50, default='', blank=True)
    image = models.URLField(null=True)
    height = models.CharField(max_length=10, default='', blank=True)
    good_point = models.CharField(max_length=100, default='', blank=True)
    like_point = models.CharField(max_length=100, default='', blank=True)
    genre = models.ManyToManyField(GenreModel, through='ActorGenreModel', through_fields=('actors', 'genres'))

    class Meta:
        db_table = 'actor'
        managed = False

    def __str__(self):
        return self.name


class ActorGenreModel(models.Model):
    id = models.AutoField(primary_key=True)
    genres = models.ForeignKey(GenreModel, on_delete=models.CASCADE, db_column='genre_id')
    actors = models.ForeignKey(ActorModel, on_delete=models.CASCADE, db_column='actor_id')

    class Meta:
        db_table = 'actor_genre'
        unique_together = (('actors', 'genres'),)
        managed = False


class FilmModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, default='')
    date = models.CharField(max_length=20, default='', blank=True)
    country = models.CharField(max_length=50, default='', blank=True)
    image = models.URLField(null=True)
    description = models.TextField(blank=True, null=True)
    time_amount = models.CharField(max_length=20, default='', blank=True)
    episode_amount = models.IntegerField(default=1, blank=True)
    view_amount = models.IntegerField(default=0)
    genre = models.ManyToManyField(GenreModel, through='FilmGenreModel', through_fields=('films', 'genres'))
    author = models.ManyToManyField(AuthorModel, through='FilmAuthorModel', through_fields=('films', 'authors'))
    actor = models.ManyToManyField(ActorModel, through='FilmActorModel', through_fields=('films', 'actors'))

    class Meta:
        db_table = 'film'
        managed = False

    def __str__(self):
        return self.name


class FilmGenreModel(models.Model):
    id = models.AutoField(primary_key=True)
    genres = models.ForeignKey(GenreModel, on_delete=models.CASCADE, db_column='genre_id')
    films = models.ForeignKey(FilmModel, on_delete=models.CASCADE, db_column='film_id')

    class Meta:
        db_table = 'film_genre'
        unique_together = (('films', 'genres'),)
        managed = False


class FilmAuthorModel(models.Model):
    id = models.AutoField(primary_key=True)
    authors = models.ForeignKey(AuthorModel, on_delete=models.CASCADE, db_column='author_id')
    films = models.ForeignKey(FilmModel, on_delete=models.CASCADE, db_column='film_id')

    class Meta:
        db_table = 'film_author'
        unique_together = (('films', 'authors'),)
        managed = False


class FilmActorModel(models.Model):
    id = models.AutoField(primary_key=True)
    actors = models.ForeignKey(ActorModel, on_delete=models.CASCADE, db_column='actor_id')
    films = models.ForeignKey(FilmModel, on_delete=models.CASCADE, db_column='film_id')

    class Meta:
        db_table = 'film_actor'
        unique_together = (('films', 'actors'),)
        managed = False


class EpisodeModel(models.Model):
    id = models.AutoField(primary_key=True)
    numberth = models.IntegerField(default=1)
    film = models.ForeignKey(FilmModel, on_delete=models.CASCADE, db_column='film_id', related_name='episodes')
    url = models.URLField()

    class Meta:
        db_table = 'episode'
        default_related_name = 'episode'
        managed = False

    def __str__(self):
        return self.film.name + " tap " + str(self.numberth)

# class CommentModel(models.Model):
#     film = models.ForeignKey(FilmModel, on_delete=models.CASCADE, db_column='phim', related_name='nhan_xet', null=True)
#     user = models.ForeignKey(settings.AUTH_USER_MODEL, db_column='nguoi_nhan_xet', on_delete=models.CASCADE)
#     comment = models.TextField(null=True)
#     date = models.DateField(auto_now_add=True)
#
#     class Meta:
#         db_table = 'comment'
#         managed = False
#
#     def __str__(self):
#         return self.comment
