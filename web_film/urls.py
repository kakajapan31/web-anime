from django.urls import path
from web_film import views

urlpatterns = [
    path('', views.FilmListView.as_view(), name='index'),
    path('help/', views.Help.as_view(), name='help'),
    path('my_account/', views.MyAccount.as_view(), name='my_account'),

    path('about_me/', views.AboutMe.as_view(), name='about_me'),
    path('login/', views.Login.as_view(), name='login'),
    path('register/', views.Register.as_view(), name='register'),
    path('logout/', views.logout, name='logout'),
    path('change_password/', views.ChangePassword.as_view(), name='change_password'),
    path('forgot_password/', views.ForgotPassword.as_view(), name='forgot_password'),

    path('film/<int:pk>/', views.FilmView.as_view(), name='film'),
    path('author/<int:pk>/', views.AuthorView.as_view(), name='author'),
    path('actor/<int:pk>/', views.ActorView.as_view(), name='actor'),
    path('genre/<int:pk>/', views.GenreView.as_view(), name='genre'),
    path('film_view/<int:pk>', views.FilmPageView.as_view(), name='film_page'),

    path('api/film/', views.FilmListCreateView.as_view(), name='api_list_film'),
    path('api/film/<int:pk>/', views.FilmDetailView.as_view(), name='api_detail_film'),
    path('api/author/', views.AuthorListView.as_view(), name='api_list_author'),
    path('api/author/<int:pk>/', views.AuthorDetailView.as_view(), name='api_detail_author'),
    path('api/actor/', views.ActorListView.as_view(), name='api_list_actor'),
    path('api/actor/<int:pk>/', views.ActorDetailView.as_view(), name='api_detail_actor'),
    path('api/genre/', views.GenreListView.as_view(), name='api_list_genre'),
    path('api/genre/<int:pk>/', views.GenreDetailView.as_view(), name='api_detail_genre'),
    path('api/film/<int:pk>/list/', views.FitmThListView.as_view(), name='api_film_list'),
    path('api/film/<int:pk>/list/<int:film_pk>', views.FilmThDetailOfFilm.as_view(),
         name='api_detail_filmth_i_of_film_j'),
]
